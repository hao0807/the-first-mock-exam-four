package com.biany.stuManager;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * a.使用 List 集合实现简易的学生信息管理系统，要求打印字符界面提示用户选择相应的功 能，根据用户输入的选择去实现
 *   增加、删除、修改、查找以及遍历所有学生信息的功能。
 * b.自定义学号异常类和年龄异常类，并在该成员变量不合理时产生异常对象并抛出。
 * c.当系统退出时将 List 集合中所有学生信息写入到文件中，当系统启动时读取文件中所 有学生信息到 List 集合中。
 */
public class Manager {

    //存放学生对象的集合
    private List<Student> studentList = null;
    //文件路径
    private String filePath = "e:/student.txt";


    /**
     * 初始化方法
     */
    public void initManager() throws IOException {
        ObjectInputStream oi = null;
        ObjectOutputStream oo = null;
        try {
            if(new File(filePath).length() == 0){  //java.io.EOFException
                oo = new ObjectOutputStream(new FileOutputStream(filePath));
                oo.writeObject(null);
            }
            oi = new ObjectInputStream(new FileInputStream(filePath));  //创建对象输入流从文件中读取Student对象
            Object obj = oi.readObject();
            //System.out.println(obj);
            if(null != obj){
                studentList = (ArrayList)obj; //读取文件中存放学生对象的集合
            }else{
                studentList = new ArrayList<>();  //如果没读到，则创建一个集合用来存放学生对象
            }
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }finally {
            if(oi != null){
                try {
                    oi.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 将集合写出到文件
     */
    public void finallyFunction() {
        ObjectOutputStream oo = null;
        try {
            oo = new ObjectOutputStream(new FileOutputStream(filePath));
            oo.writeObject(studentList);  //写入集合
        } catch (IOException e) {
            e.printStackTrace();
        } finally{
            if(oo != null){
                try {
                    oo.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * 查看学生信息
     * @param name
     */
    public void selectStu(String name){
        boolean flag = true;
        for(Student stu : studentList){
            if(name.equals(stu.getName())){
                flag = false;
                System.out.println(name+"同学信息如下："+stu.toString());
                break;
            }
        }
        //系统不存在name同学的信息
        if(flag){
            System.out.println("系统没有"+name+"同学的信息！");
        }
        System.out.println("请选择你要使用的功能(输入各功能代表的字母)");
    }


    /**
     * 删除学生信息
     */
    public void deleteStu(String name){
        boolean flag = true;
        for(Student stu : studentList){
            if(name.equals(stu.getName())){
                flag = false;
                studentList.remove(stu);
                System.out.println("删除成功！");
                break;
            }
        }
        //系统不存在name同学的信息
        if(flag){
            System.out.println("系统没有"+name+"同学的信息！");
        }
        System.out.println("请选择你要使用的功能(输入各功能代表的字母)");
    }

    /**
     * 修改学生信息
     */
    public void editStu(String name, Scanner sc) throws AgeException, IdException {
        boolean flag = true;
        temp:for(Student stu : studentList){
            if(name.equals(stu.getName())){
                flag = false;
                while(true){
                    System.out.println("请输入要修改的信息名称(年龄、学号),退出修改功能请输入exit");
                    String stuInfo = sc.next();
                    if("姓名".equals(stuInfo)){
                        System.out.println("请输入修改之后的名字");
                        String newName = sc.next();
                        stu.setName(newName);
                    }else if("年龄".equals(stuInfo)){
                        System.out.println("请输入修改之后的年龄");
                        int newAge = sc.nextInt();
                        stu.setAge(newAge);
                    }else if("学号".equals(stuInfo)){
                        System.out.println("请输入修改之后的名字");
                        int newId = sc.nextInt();
                        stu.setId(newId);
                    }else if("exit".equals(stuInfo)){
                        break temp;  //直接退出外层循环
                    }else {
                        System.out.println("请输入正确的信息名称！");
                    }

                }
            }
        }

        //系统不存在name同学的信息
        if(flag){
            System.out.println("系统没有"+name+"同学的信息！");
        }
        System.out.println("请选择你要使用的功能(输入各功能代表的字母)");
    }


    /**
     * 新增学生信息
     */
    public void addStu(String name, Scanner sc) throws AgeException, IdException {
        boolean flag = true;
        for(Student stu : studentList){
            if(name.equals(stu.getName())){
                flag = false;
                System.out.println(name+"同学信息已存在！");
                break;
            }
        }

        if(flag){
            Student student = new Student();
            student.setName(name);
            while(true){
                System.out.println("请输入要新增的的信息名称(年龄、学号),退出新增功能请输入exit");
                String stuInfo = sc.next();
                if("年龄".equals(stuInfo)){
                    System.out.println("请输入年龄");
                    int newAge = sc.nextInt();
                    student.setAge(newAge);
                }else if("学号".equals(stuInfo)){
                    System.out.println("请输入的学号");
                    int newId = sc.nextInt();
                    student.setId(newId);
                }else if("exit".equals(stuInfo)){
                    break;  //直接退出外层循环
                }else {
                    System.out.println("请输入正确的信息名称！");
                }
            }
            studentList.add(student);
            System.out.println(name+"同学的信息添加成功！");
        }
        System.out.println("请选择你要使用的功能(输入各功能代表的字母)");
    }

    /**
     * 界面功能
     * @throws AgeException
     * @throws IdException
     */
    public void function() throws AgeException, IdException {
        Scanner sc = new Scanner(System.in);
        String option = null;
        String stuName = null;
        System.out.println("---------------------------欢迎来到德莱联盟---------------------------------");
        System.out.println("请选择你要使用的功能(输入各功能代表的字母)");
        System.out.println("A:查看学生信息    B:修改学生信息    C:添加学生信息    D:删除学生信息    E:退出系统");
        while(true){
            option = sc.next();
            if("A".equalsIgnoreCase(option)){
                System.out.println("请输入要查看的学生姓名:");
                while(true){  //循环输入
                    stuName = sc.next();
                    if(null != stuName && "" != stuName.trim()){  //校验输入的姓名是否为空
                        selectStu(stuName);
                        break;
                    }else{
                        System.out.println("请重新正确的姓名！");
                    }
                }

            }else if("B".equalsIgnoreCase(option)){
                System.out.println("请输入要修改的学生姓名:");
                while(true){  //循环输入
                    stuName = sc.next();
                    if(null != stuName && "" != stuName.trim()){  //校验输入的姓名是否为空
                        editStu(stuName, sc);
                        break;
                    }else{
                        System.out.println("请重新正确的姓名！");
                    }
                }
            }else if("C".equalsIgnoreCase(option)){
                System.out.println("请输入要新增的学生姓名:");
                while(true){  //循环输入
                    stuName = sc.next();
                    if(null != stuName && "" != stuName.trim()){  //校验输入的姓名是否为空
                        addStu(stuName ,sc);
                        break;
                    }else{
                        System.out.println("请重新正确的姓名！");
                    }
                }
            }else if("D".equalsIgnoreCase(option)){
                System.out.println("请输入要删除的学生姓名:");
                while(true){  //循环输入
                    stuName = sc.next();
                    if(null != stuName && "" != stuName.trim()){  //校验输入的姓名是否为空
                        deleteStu(stuName);
                        break;
                    }else{
                        System.out.println("请重新正确的姓名！");
                    }
                }
            }else if("E".equalsIgnoreCase(option)){
                finallyFunction(); //将存放学生对象的集合写入文件
                break;
            }else{
                System.out.println("您输入的选项有误，请重新输入！！！");
            }
        }

        System.out.println("一路走好，欢迎下次登录！");
    }


    public static void main(String[] args){

        Manager manager = new Manager();
        try {
            manager.initManager();
            manager.function();
        } catch (AgeException | IdException | IOException e) {
            e.printStackTrace();
        }
    }
}
