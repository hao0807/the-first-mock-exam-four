package com.biany.stuManager;

/**
 * 学号异常类
 */
public class IdException extends Exception{

    public IdException() {
    }

    public IdException(String message){
        super(message);
    }
}
