package com.biany.stuManager;

import java.io.Serializable;
import java.util.Objects;

/**
 * 学生类
 */
public class Student implements Serializable {

    private static long seriaVersionUID = 1L;  //序列化版本号

    private String name; //姓名
    private int age;  //年龄
    private int id;  //学号

    public Student() {
    }

    public Student(String name, int age, int id) {
        this.name = name;
        this.age = age;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    //setAge()判断年龄是否合理，不合理抛出年龄不合理异常
    public void setAge(int age) throws AgeException {
        if(age > 0){
            this.age = age;
        }else{
            throw new AgeException("年龄不合理！");
        }
    }

    public int getId() {
        return id;
    }

    //setId()判断学号输入是否合理，不合理抛出学号不合理异常
    public void setId(int id) throws IdException {
        if(id > 0){
            this.id = id;
        }else{
            throw new IdException("学号不合理！");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return age == student.age &&
                id == student.id &&
                Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, id);
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", id=" + id +
                '}';
    }
}
