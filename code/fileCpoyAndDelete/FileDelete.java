package com.biany.fileCpoyAndDelete;

import java.io.File;

/**
 *  b.实现将指定目录中的所有内容删除，包含子目录中的内容都要全部删除。
 */
public class FileDelete implements Runnable{

    private String filePath;   //指定的目录

    public FileDelete() {
    }

    public FileDelete(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public void run() {
        File file = new File(filePath);
        if(!file.exists()){
            System.out.println("指定目录不存在");
        }
        deleteFile(file);
    }


    /**
     * 递归删除子目录和文件
     * @param file
     */
    public void deleteFile(File file){
        File[] files = file.listFiles();
        for(File temp : files){
            if(temp.isFile()){
                temp.delete();  //文件直接删除
                System.out.println(temp.getAbsolutePath()+"已删除");
            }
            if(temp.isDirectory()){ //文件夹
                if(temp.listFiles().length > 0){
                    deleteFile(temp);  //非空文件夹递归删除
                }else{
                    temp.delete();  //空文件夹直接删除
                    System.out.println(temp.getAbsolutePath()+"已删除");
                }

            }
        }
        //删除最外层的文件夹
        file.delete();
        System.out.println(file.getAbsolutePath()+"已删除");
    }
}
