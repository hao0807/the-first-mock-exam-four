package com.biany.fileCpoyAndDelete;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FileCopyAndDeleteTest {

    public static void main(String[] args){
        //创建线程池
        ExecutorService es = Executors.newCachedThreadPool();
        FileCopy fc = new FileCopy("f:/a","f:/w");
        FileDelete fd = new FileDelete("f:/w");
        //启动拷贝线程
        //es.execute(fc);
        //启动删除线程
        es.execute(fd);
        //有序关闭启动线程
        es.shutdown();

    }
}
