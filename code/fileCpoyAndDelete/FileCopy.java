package com.biany.fileCpoyAndDelete;

import java.io.*;

/**
 *  a.使用线程池将一个目录中的所有内容拷贝到另外一个目录中，包含子目录中的内容。
 */
public class FileCopy implements Runnable{

    private String formFilePath;  //源文件目录
    private String targetFilePath;  //要拷贝的目标目录
    BufferedInputStream bis = null;
    BufferedOutputStream bos = null;

    public FileCopy() {
    }
    public FileCopy(String formFilePath, String targetFilePath) {
        this.formFilePath = formFilePath;
        this.targetFilePath = targetFilePath;
    }

    @Override
    public void run() {
        File formFile = new File(formFilePath);  //源文件
        File targetFile = new File(targetFilePath);  //目标文件
        if(!targetFile.exists()){  //若目标文件夹不存在则创建
            targetFile.mkdir();
        }
        createNewFile(targetFilePath, formFile);

    }

    /**
     * 递归查询创建
     * @param fileName
     * @param formFile
     */
    public void createNewFile(String fileName ,File formFile){
        File[] formFlies = formFile.listFiles();  //源文件夹下的目录
        for(File temp : formFlies){
            File newFile = new File(fileName,temp.getName());  //使用目标路径+新建的文件名
            if(temp.isFile()){  //文件直接创建
                try {
                    newFile.createNewFile();
                    System.out.println("创建文件："+newFile.getAbsolutePath());
                    //拷贝文件的内容
                    bis = new BufferedInputStream(new FileInputStream(temp));  //源文件
                    bos = new BufferedOutputStream(new FileOutputStream(newFile));  //目标文件
                    byte[] bytes = new byte[1024 * 8];
                    while(bis.read(bytes, 0, bytes.length) != -1){
                        bos.write(bytes, 0, bytes.length);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally{
                  if(bos != null){
                      try {
                          bos.close();
                      } catch (IOException e) {
                          e.printStackTrace();
                      }
                  }
                  if(bis != null){
                      try {
                          bis.close();
                      } catch (IOException e) {
                          e.printStackTrace();
                      }
                  }
                }
            }
            if(temp.isDirectory()){  //文件夹先创建后递归找子文件夹
                newFile.mkdir();
                System.out.println("创建文件夹："+newFile.getAbsolutePath());
                createNewFile(newFile.getAbsolutePath(), temp);  //递归调用，创建子文件夹
            }
        }
    }

}
