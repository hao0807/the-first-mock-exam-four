package com.biany.TCPChat;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

/**
 * 客户端2
 */
public class TCPClient_2 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Socket socket = null;
        PrintWriter pw = null;
        BufferedReader br = null;
        String chat = null;
        try {
            socket = new Socket("localhost",8888);
            System.out.println(Thread.currentThread().getName()+"客户请说出你想说的话:");
            pw = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            while(!"bye".equals(chat = sc.nextLine())){
                //向服务器发数据
                pw.println(chat);  //向服务器打印
                pw.flush();  //刷新流
                //读取客户端发来的信息
                String str = br.readLine();
                System.out.println("服务器发来的信息："+str);

            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally{
            if(br != null){
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(pw != null){
                pw.close();
            }
            if(socket != null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
