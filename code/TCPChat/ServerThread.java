package com.biany.TCPChat;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;


/**
 * 服务器进程
 */
public class ServerThread implements Runnable{

    private String chat;  //客户端发来的信息，同步信息
    private Socket socket;
    private List<Socket> socketList;  //其他socket

    public ServerThread() {
    }

    public ServerThread(Socket socket, List<Socket> socketList) {
        this.socket = socket;
        this.socketList = socketList;
    }

    @Override
    public void run(){
        ServerSocket ss = null;
        BufferedReader br = null;
        PrintWriter pw = null;
        try {
            System.out.println(socket.getInetAddress()+"客户端连接成功！");
            while(true){
                br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                //接收客户端socket发来的信息
                chat = br.readLine();
                System.out.println("客户端"+socket.getInetAddress()+"发来:"+chat);
                //向所有在线的客户端发消息
                for(Socket temp : socketList){
                    pw = new PrintWriter(new OutputStreamWriter(temp.getOutputStream()));
                    pw.println(chat);
                    pw.flush();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally{
            if(pw != null){
                pw.close();
            }
            if(br != null){
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(socket != null){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if(ss != null){
                try {
                    ss.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}

