package com.biany.TCPChat;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 使用基于 tcp 协议的编程模型实现多人同时在线聊天，要求每个客户端将发 送的聊天内容发送到服务器，
 * 服务器接收到后转发给当前所有在线的客户端。
 */
public class TCPServer {

    public static void main(String[] args){
        try {
            List<Socket> socketList = new ArrayList<>();  //装多个socket
            ExecutorService es = Executors.newCachedThreadPool();
            ServerSocket ss = new ServerSocket(8888);
            System.out.println("等待客户端的连接...");
            while(true){
                Socket socket = ss.accept();
                socketList.add(socket);
                es.execute(new ServerThread(socket,socketList));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally{

        }
    }
}
